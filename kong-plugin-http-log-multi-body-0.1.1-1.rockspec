package = "kong-plugin-http-log-multi-body"
version = "0.1.1-1"
source = {
  url = "git://gitlab.com/yotsu/kong-plugin-http-log-multi-body",
  branch = "master"
}
description = {
  summary = "This plugin allows Kong to send log using HTTP request."
}
build = {
  type = "builtin",
  modules = {
    ["kong.plugins.http-log-multi-body.handler"] = "kong/plugins/http-log-multi-body/handler.lua",
    ["kong.plugins.http-log-multi-body.schema"]  = "kong/plugins/http-log-multi-body/schema.lua",
  }
}
